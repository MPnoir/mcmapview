package com.noir.mcmapview;
/**
 * This program a simple Viewer for Minecraft Maps (the ones creathed by the Map Item)
 * @author Florian "noir" Hageneier
 * @version 0.1 (11.07.2013) 
 */


import java.awt.Container;
import java.awt.Image;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JFrame;

public class McMapView {
	private JFrame frame;
	private int scale;
	private Tag rootTag;
	private short mapWidth, mapHeight;
	private byte[][] colorData;
	
	
	/**
	 * Constructor for the main program
	 * @param pFilename The filename of the map file
	 * @param pScale The scale the map will be shown with. Default is 4
	 * 
	 */
	public McMapView(String pFilename, int pScale){
		openFile(pFilename);
		this.scale = pScale;
		byte[] colorDataRaw = null;
		
		try{
			//Try to get data from the file
			this.mapWidth = (Short) this.rootTag.findTagByName("data").findTagByName("width").getValue();
			this.mapHeight = (Short) this.rootTag.findTagByName("data").findTagByName("height").getValue();
			this.colorData = new byte[this.mapHeight][this.mapWidth];
			colorDataRaw = (byte[]) this.rootTag.findTagByName("data").findTagByName("colors").getValue();
		}catch(Exception e){
			//File isn't a map file
			System.out.println("Error: The file isn't a minecraft map file!\nPlease choose a different one");
			System.exit(1);
		}
		
		//Output basic data to stdout
		System.out.println("\tMap Height: "+this.mapHeight);
		System.out.println("\tMap Width: "+this.mapWidth);
		System.out.println("\tScale: "+this.scale);
		
		//Convert the 1-dimensional byte array to a 2-dimensional one
		for(int y=0; y<this.mapHeight; y++){
			for(int x=0; x<this.mapWidth; x++){
				this.colorData[y][x] = colorDataRaw[(this.mapWidth*y)+x];
			}
		}
		
		String[] mapNameSplit = pFilename.split("/");
		String mapName = mapNameSplit[mapNameSplit.length-1];
		
		//Create the frame and set some things
		this.frame = new JFrame(mapName+" - McMapView");
		this.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.frame.setBounds(0, 0, this.mapWidth*this.scale, this.mapHeight*this.scale);
		this.frame.setResizable(false);
		//Try to set an icon
		try {
			Image icon = ImageIO.read(getClass().getResource("res/icon.png"));
			this.frame.setIconImage(icon);
		} catch (IOException e) {
			//nothing
		}
		
		//Create a content pane
		Container contentPane = new Container();
		contentPane.setLayout(null);
		this.frame.setContentPane(contentPane);
		
		//Create a MapView with the color data from the file
		MapView mapView = new MapView(this.colorData);
		mapView.setBounds(0, 0, this.mapWidth*this.scale, this.mapHeight*this.scale);
		
		contentPane.add(mapView);
		this.frame.setVisible(true);
	}
	
	/**
	 * This method opens a file and tries to read NBT Tags from it
	 * @param pFilename The filename of the file to open
	 */
	private void openFile(String pFilename){
		try{
			File fh = new File(pFilename);
			if(fh.exists()){
				FileInputStream is = new FileInputStream(fh);
				this.rootTag = Tag.readFrom(is);
				System.out.println("Successfully opened "+pFilename);
			}else{
				System.out.println("Error while opening the File: File does not exist!\nPlease choose a different one");
				System.out.println(pFilename);
				System.exit(1);
			}	
		}catch(Exception e){
			System.out.println("An Error occoured while trying to open the File!");
			e.printStackTrace();
			System.exit(1);
		}
	}
	
	/**
	 * Main method
	 */
	public static void main(String args[]){
		if(args.length == 1){
			new McMapView(args[0], 4);
		}else if(args.length == 2){
			int scale=0;
			try{
				scale = Integer.parseInt(args[1].trim());
			}catch(Exception e){
				System.out.println("Invalid argument for scale. Please enter a number!");
				e.printStackTrace();
				System.exit(1);
			}
			new McMapView(args[0], scale);
		}else{
			System.out.println("USAGE: mcmapview filename [interval]");
			System.exit(1);
		}
	}

}