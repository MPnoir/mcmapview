package com.noir.mcmapview;
/**
 * This program a simple Viewer for Minecraft Maps (the ones creathed by the Map Item)
 * @author Florian "noir" Hageneier
 * @version 0.1 (11.07.2013) 
 */

import java.awt.Color;

final class MapColors{
	/**
	 * Array of map colors
	 * Made from list at the minecraftwiki
	 * @see <a href="http://www.minecraftwiki.net/wiki/Map_item_format#Map_colors">Map Colors List</a>
	 */
	public static final Color[] colors = {
			new Color(0,0,0), //0
			new Color(0,0,0),
			new Color(0,0,0),
			new Color(0,0,0),
			new Color(89,125,39),
			new Color(109,153,48), //5
			new Color(127,178,56),
			new Color(109,153,48),
			new Color(174,164,115),
			new Color(213,201,140),
			new Color(247,233,163), //10
			new Color(213,201,140),
			new Color(117,117,117),
			new Color(144,144,144),
			new Color(167,167,167),
			new Color(144,144,144), //15
			new Color(180,0,0),
			new Color(220,0,0),
			new Color(255,0,0),
			new Color(220,0,0),
			new Color(112,112,180), //20
			new Color(138,138,220),
			new Color(160,160,255),
			new Color(138,138,220),
			new Color(117,117,117),
			new Color(144,144,144), //25
			new Color(167,167,167),
			new Color(144,144,144),
			new Color(0,87,0),
			new Color(0,106,0),
			new Color(0,124,0), //30
			new Color(0,106,0),
			new Color(180,180,180),
			new Color(220,220,220),
			new Color(255,255,255),
			new Color(220,220,220), //35
			new Color(115,118,129),
			new Color(141,144,158),
			new Color(164,168,184),
			new Color(141,144,158),
			new Color(129,74,33), //40
			new Color(157,91,40),
			new Color(183,106,47),
			new Color(157,91,40),
			new Color(79,79,79),
			new Color(96,96,96), //45
			new Color(112,112,112),
			new Color(96,96,96),
			new Color(45,45,180),
			new Color(55,55,220),
			new Color(64,64,255), //50
			new Color(55,55,220),
			new Color(73,58,35),
			new Color(89,71,43),
			new Color(104,83,50),
			new Color(89,71,43) //55
	};
}