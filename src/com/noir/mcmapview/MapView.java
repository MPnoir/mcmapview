package com.noir.mcmapview;
/**
 * This program a simple Viewer for Minecraft Maps (the ones creathed by the Map Item)
 * @author Florian "noir" Hageneier
 * @version 0.1 (11.07.2013) 
 */

import java.awt.Graphics;
import javax.swing.JPanel;

/**
 * JPanel to show map in
 */
class MapView extends JPanel{
	private static final long serialVersionUID = -3790919600884817047L;
	private byte[][] data;
	
	/**
	 *Constructor
	 *@param pData The map color data read from the NBT file 
	 */
	public MapView(byte[][] pData){
		super();
		this.data = pData;
	}
	
	@Override
	protected void paintComponent(Graphics g){
		super.paintComponent(g);
		//Calculate the size each Pixel has by dividing the size of the JPanel by the number of elements in the data array
		int boxWidth  = this.getWidth()/this.data[0].length;
		int boxHeight  = this.getHeight()/this.data.length;
		
		//Draw each Pixel
		for(int i1=0; i1<this.data.length; i1++){
			for(int i2=0; i2<this.data[0].length; i2++){
				g.setColor(MapColors.colors[this.data[i1][i2]]);
				g.fillRect(i1*boxWidth, i2*boxHeight, boxWidth, boxHeight);
			}
		}
	}
}
